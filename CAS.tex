\documentclass{article}

\title{Transition to GIT for large software projects}
\author{Claudia Fellinger, Boris Prohaska, Karthik Sukumar Nambiar}
\date{}
\usepackage{listings}
\usepackage{graphicx}

\begin{document}
\maketitle
\begin{abstract}

GIT is a distributed revision control system with history and version tracking capability. Originally developed in 2005 for the Linux kernel development it is free for use and published under the GNU General Public License Version 2.
Main advantages of GIT are that it is fast and scalable and therefor suitable for large software repositories although it was not explicitly designed for huge projects.
The aim of this paper is to evaluate the advantages of GIT versus a client-server revision control system like e.g. Perforce or SVN and to give guidelines on how such a transition could take place.

\end{abstract}
\tableofcontents

\newpage
\section{Introduction}

Revision control systems are software tools which help to manage and maintain software projects. It allows several developers to work on the same source code. Changes are usually identified by a unique code of letters and numbers. Each change is also tagged with a timestamp and who committed the code change. The uniqueness of the code allows it to identify an individual change and therefor restore an older version. Besides restoring it is also possible to merge, compare and organize various code revision remotely. The evolvement over the lifetime of the project can be made visibly.
 
There are different approaches to revision control systems. Centralized systems work like a client server model. There is one central copy of the source code or project on a server and changes to this code are committed to this central copy. Developers can easily retrieve the code or rather any version of the code whenever needed. So there is no need to keep the files locally anytime. Known representatives of centralized revision control tools are SVN (Subversion) and Perforce.

Since the mid 2000 years a new approach to revision control became popular. Distributed systems found its way into version control. These systems do not store a single centralized copy of the repository but every developer has his or her own cloned working copy including all the metadata like the history. These copies are synchronized among themselves. An advantage is that because there is no need to communicate with a central server operations like commits and reverting changes are faster.
Commits are made at the local repository without affecting any other repositories and a push afterward distributes the changes. Pushing and pulling operations therefor are slower than commits and can also be done without a connection to the internet or local area network.
Another advantage is that local commits are invisible to others before there are pushed.
A disadvantage is that for files with a long history the download of the history will take longer.
Examples for distributed revision control systems are GIT and Fossil.

\section{Repository Structure}

For CI (Continuous Integration) and feature branching implementations a fine grained repository structure usually is the best approach. Due to existing structures and project timelines this might not be achieved in a first step of the transfer.
It is not unusual that a repository consist of thousands of files with often multiple dependencies. In many repositories there is also generated code checked in which also makes no sense to keep this structure in GIT because generated code will let the size of the repository grow intensively. GIT only saves the changes of a file as it cannot make diffs and merges for binary files it will upload the entire binary file and store it in the repository each time a pull is made. This may result in an increasing amount of data a developer has to download while pulling a repository will increase.
The key concept of CI is that if developers typically work on particular parts of the code and different implementation stories it will make sense to build and test only these parts. For huge software projects that saves time and decreases the error liability. If a lot of developers work on the same source code it will be difficult to find a code error a couple of days later.
In a fine grained repository structure the repositories are as small and defined as possible. This will lead to a flexible, scalable and clearly arranged structure. Developers will need to download less code and CI and feature branching will be achieved easier. A disadvantage is the initial migration effort. Build files will have to be changed heavily and development workflows have to be rearranged. Branching across repositories also has to be implemented. Achieving a fine grained structure is mainly a question of time.
If time is a crucial factor it is possible to keep the current structure in GIT and purge this afterwards in a later project. On the other hand side this will to no major benefits. The old grown and often unclear repository structure will be kept and a CI implementation will not benefit more from this old structure than before the migration to GIT. Feature branching will be possible though.
As time is always an important factor the migration strategy will usually be a partial structure resolving. Meaning that the parts which can be resolved easily will be resolved during the migration to GIT and the more difficult parts will be resolved in a later project. This approach reduces the risks of the GIT migration but leads a way on how to proceed afterwards.
The following steps can be recommended to identify the final repository structure.

\begin{enumerate}
\item Identify current repository structure and dependencies
\item Identify generated code
\item Identify components which can be dissolved easily
\end{enumerate}

It is common practice to define so-called sub-modules (which are independent git repositories on their own) to cope with this situation.


\section{Why GIT?}
Switching from a centralized version control system to GIT changes the way a development team creates software. One of the biggest advantages of GIT is its branching capabilities. Unlike centralized version control systems, GIT branches are cheap and easy to merge. This facilitates the feature branch workflow popular with many GIT users. Feature branches provide an isolated environment for every change to the existing codebase. When a developer wants to start working on something - no matter how big or small - they create a new branch. This ensures that the master branch always contains production-quality code. Using feature branches is not only more reliable than directly editing production code, but it also provides organizational benefits. They let represent development work at the same granularity as the agile backlog. \cite{GITSCM}

\subsection{Distributed Development}

In SVN, each developer gets a working copy that points back to a single central repository. GIT, however, is a distributed version control system. Instead of a working copy, each developer gets their own local repository, complete with a full history of commits. Having a full local history makes GIT fast, since it means it isn't required to have a network connection to create commits, inspect previous versions of a file, or perform diffs between commits. Distributed development also makes it easier to scale an engineering team. If someone breaks the production branch in SVN, other developers can?t check in their changes until it?s fixed. With GIT, this kind of blocking doesn?t exist. Everybody can continue going about their business in their own local repositories. And, similar to feature branches, distributed development creates a more reliable environment. Even if a developer obliterates their own repository, they can simply clone someone else?s and start anew.

\subsection{Pull Requests}
Many source code management tools such as Bitbucket enhance core GIT functionality with pull requests. A pull request is a way to ask another developer to merge one of another developers branches into their repository. This not only makes it easier for project leads to keep track of changes, but also lets developers initiate discussions around their work before integrating it with the rest of the codebase. Since they?re essentially a comment thread attached to a feature branch, pull requests are extremely versatile. When a developer gets stuck with a hard problem, they can open a pull request to ask for help from the rest of the team. Alternatively, junior developers can be confident that they aren?t destroying the entire project by treating pull requests as a formal code review.

\subsection{Community}
In many circles, GIT has come to be the expected version control system for new projects. If a team is using GIT, odds are one won?t have to train new hires on this workflow, because they?ll already be familiar with distributed development. In addition, GIT is very popular among open source projects. This means it?s easy to leverage 3rd-party libraries and encourage others to fork open source code.

\subsection{Faster Release Cycle}
The ultimate result of feature branches, distributed development, pull requests, and a stable community is a faster release cycle. These capabilities facilitate an agile workflow where developers are encouraged to share smaller changes more frequently. In turn, changes can get pushed down the deployment pipeline faster than the monolithic releases common with centralized version control systems. GIT works very well with continuous integration and continuous delivery environments. GIT hooks allow to run scripts when certain events occur inside of a repository. It is even possible to build or deploy code from specific branches to different servers.

\subsection{For Designers}
Feature branches lend themselves to rapid prototyping. Whether UX/UI designers want to implement an entirely new user flow or simply replace some icons, checking out a new branch gives them a sandboxed environment to play with. This lets designers see how their changes will look in a real working copy of the product without the threat of breaking existing functionality.

\section{Migration Strategy}
\subsection{Big Bang vs. Stepwise}
There are basically two approaches for a migration to GIT. 
\begin{enumerate}
\item Big Bang - the transition is "hard" - a switch is made from one day to the other with no possible return
\item Stepwise - GIT allows to be introduced partially and be expanded and integrated as the project moves on
\end{enumerate}
We will be focussing on the "Big Bang" approach as it takes care of everything at once, and developers can start taking full advantage of GIT from the very beginning of the transition.
\subsection{Migrating from different VCS}
The following chapter will show a hands on approach on how to migrate from both Perforce and SVN to GIT.
\subsubsection{Migrating from Perforce}
Perforce launched a very nice tool called GIT Fusion. \cite{GITFUSION} With GIT Fusion one can work with a GIT client without knowing that there is a Perforce server behind it. When a GIT user pushes a change in the repo, GITFusion translates those GIT changes into Perforce changes and submits them to the Perforce depot. And when a Perforce user submits a change to the Perforce depot GIT Fusion translates those changes to the GIT repository. GIT Fusion is a powerful tool that allows the developer to keep using Perforce through it. In this case we want to use the capability to move to GIT entirely. So a configuration must be written to accomplish this. Of course, GIT Fusion must be installed - the process depends on the operating system. In Linux, we can download the corresponding RPM and install it directly. Once GIT Fusion is configured, there will be a new Perforce user called git-fusion-user. A new Perforce depot in //.git-fusion will be automatically created too. After taking care of the access rights to the repository (usually via SSH-key) the migration configuration can start.
Then, a configuration file is being created: //.git-fusion/repos/repo/migration\textunderscore config for each repo that is being migrated. A sample for a configuration can be found below:
\lstset{language={XML},caption={Migration config from Perforce },label=perforce]}
\begin{lstlisting}
[@repo]
description = Repo for Migration

[master]
git-branch-name = master
stream = //repo/mainline
original-view = //repo/mainline/...

[feature]
git-branch-name = feature/feature
stream = //repo/feature
original-view = //myrepo/myfeature/...
\end{lstlisting}

To clone the GIT repo one can do it like a normal GIT repo with the git clone command:
\lstset{language={XML},caption={Cloning the new Repository },label=perforceclonegitnew]}
\begin{lstlisting}
git clone git@git.technikum-wien.at:repo
\end{lstlisting}
Notice that the first time that one clones the repository GIT Fusion will translate all the changes to the GIT repository, so it can take several minutes or hours depending on the size of the repository. After that, the full history is in GIT, and the user can work exclusively in GIT from here on. 

\subsubsection{Migrating from SVN}
Migration from SVN is a little bit trickier, because of the completely different nature of both VC systems. While there are tools available that take care of this process with simple repositories \cite{SVNTOOL}, we will provide a comprehensive guide to migrate a more complex repository.

A users file needs to be created first (i.e. users.txt) for mapping SVN users to GIT:

\lstset{language={XML},caption={users.txt },label=svnusers]}
\begin{lstlisting}
user1 = First Last Name <email@technikum-wien.com>
user2 = First Last Name <email@technikum-wien.com>
...
\end{lstlisting}

As an alternative this one-liner can be used to build a template from an existing SVN repository:
\lstset{language={XML},caption={Building a user template },label=svnusers]}
\begin{lstlisting}
svn log --xml | 
grep author | 
sort -u | 
perl -pe 's/.*>(.*?)<.*/$1 = /' | tee users.txt
\end{lstlisting}

SVN will stop if it finds a missing SVN user not in the file. But after that one can update the file and pick-up where one left off.

Now the SVN data needs to be pulled from the repository:
\lstset{language={XML},caption={Initial pull},label=initialpull]}
\begin{lstlisting}
git svn clone 	--stdlayout 
		--no-metadata 
		--authors-file=users.txt 
		svn://hostname/path
		destdir-tmp
\end{lstlisting}

This command will create a new GIT repository in destdir-tmp and start pulling the SVN repository. Note that the "--stdlayout" flag implies that one has the common "trunk/, branches/, tags/" SVN layout. If the layout differs, --tags, --branches, --trunk options need to be used.
All common protocols are allowed: svn://, http://, https://. The URL should target the base repository, something like http://svn.technikum-wien.at/repo/repository. That must not include /trunk, /tag or /branches.
Note that after executing this command it very often looks like the operation is "hanging/freezed", and it's quite normal that it can be stuck for a long time after initializing the new repository. Eventually there are log messages which indicate that it's migrating.
Also note that if  the --no-metadata flag is omitted, GIT will append information about the corresponding SVN revision to the commit message (i.e. git-svn-id: svn://svn.technikum-wien.at/repo/\textless branchname/trunk\textgreater @\textless RevisionNumber\textgreater \textless Repository UUID\textgreater )
If a user name is not found, update the users.txt file. After that we need to go to our temporary destination directory and fetch all commits from SVN using these commands:
\lstset{language={XML},caption={Fetch all commits},label=fetchcommits]}
\begin{lstlisting}
cd destdir-tmp
git svn fetch
\end{lstlisting}

It might be needed to repeat that last command several times, if the project is large, until all of the Subversion commits have been fetched.
When completed, GIT will checkout the SVN trunk into a new branch. Any other branches are setup as remotes. We can view the other SVN branches with:
\lstset{language={XML},caption={View SVN branches},label=viewsvnbranches]}
\begin{lstlisting}
git branch -r
\end{lstlisting}

If we want to keep other remote branches in our repository, we want to create a local branch for each one manually. (Skip trunk/master.) If we don't do this, the branches won't get cloned in the final step.
\lstset{language={XML},caption={Keeping remote branches},label=krb]}
\begin{lstlisting}
git checkout -b localbranch remotebranch
#It is OK if localbranch and 
#remotebranch are the same name
\end{lstlisting}

Tags are imported as branches. We have to create a local branch, make a tag and delete the branch to have them as tags in GIT. To do it with tag "v1":
\lstset{language={XML},caption={Creating local branches for tags},label=clbft]}
\begin{lstlisting}
git checkout -b tagv1 remotes/tags/v1
git checkout master
git tag v1 tagv1
git branch -D tagv1
\end{lstlisting}

Now we clone our GIT-SVN repository into a clean GIT repository:
\lstset{language={XML},caption={Clone to clean repo},label=ctcr]}
\begin{lstlisting}
git clone destdir-tmp destdir
rm -rf destdir-tmp
cd destdir
\end{lstlisting}

The local branches that we created earlier from remote branches will only have been copied as remote branches into the new cloned repository. (Skip trunk/master.) For each branch we want to keep:
\lstset{language={XML},caption={Keeping local branches},label=klb]}
\begin{lstlisting}
git checkout -b localbranch origin/remotebranch
\end{lstlisting}

Finally, remove the remote from our clean GIT repository that points to the now deleted temporary repository:
\lstset{language={XML},caption={Remove the wrong remote},label=ctcr]}
\begin{lstlisting}
git remote rm origin
\end{lstlisting}

After this - we are ready. This completes the migration process from SVN.

\section{Conclusion}
Depending on the repository size that one is coming from, these migration processes can take a few minutes to several weeks. Especially, when there is a bigger codebase from different developers, everyone needs to go through the migration process separately, to ensure that no code is being lost. As a general rule it is advised, that the old repository is kept in "read-only" mode - as a backup, if something goes wrong. We would recommend to start every project with GIT, as it became the de-facto standard in VCS. 

\begin{thebibliography}{1}

  \bibitem{GITFUSION} GIT FUSION GUIDE (2016.2)  {\em   https://www.perforce.com/perforce/doc.current/manuals/git-fusion/2}  2016.

  \bibitem{SUBGIT} Svn To Git Migration {\em http://www.subgit.com}  2016.

  \bibitem{GITSCM} The GIT Project {\em https://git-scm.com}  2016.



  \end{thebibliography}

\lstlistoflistings

\end{document}