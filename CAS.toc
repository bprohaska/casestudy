\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Repository Structure}{2}
\contentsline {section}{\numberline {3}Why GIT?}{3}
\contentsline {subsection}{\numberline {3.1}Distributed Development}{4}
\contentsline {subsection}{\numberline {3.2}Pull Requests}{4}
\contentsline {subsection}{\numberline {3.3}Community}{4}
\contentsline {subsection}{\numberline {3.4}Faster Release Cycle}{4}
\contentsline {subsection}{\numberline {3.5}For Designers}{5}
\contentsline {section}{\numberline {4}Migration Strategy}{5}
\contentsline {subsection}{\numberline {4.1}Big Bang vs. Stepwise}{5}
\contentsline {subsection}{\numberline {4.2}Migrating from different VCS}{5}
\contentsline {subsubsection}{\numberline {4.2.1}Migrating from Perforce}{5}
\contentsline {subsubsection}{\numberline {4.2.2}Migrating from SVN}{6}
\contentsline {section}{\numberline {5}Conclusion}{8}
